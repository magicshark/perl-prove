/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myorg.perl_prove;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import static org.netbeans.api.editor.EditorRegistry.lastFocusedComponent;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.InputOutput;

@ActionID(
        category = "Build",
        id = "org.myorg.perl_prove.ProveActionListener"
)
@ActionRegistration(
        iconBase = "org/myorg/perl_prove/feu.png",
        displayName = "#CTL_ProveActionListener"
)
@ActionReferences({
    @ActionReference(path = "Menu/RunProject", position = 100, separatorBefore = 50, separatorAfter = 150),
    @ActionReference(path = "Toolbars/Build", position = -20)
})
@Messages("CTL_ProveActionListener=Prove")
public final class ProveActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent ed = lastFocusedComponent();
        Document doc = ed.getDocument();
        try {
            // TODO implement action body
            Runtime runtime = Runtime.getRuntime();
            final Process process = runtime.exec(new String[] {"prove.bat",(String) doc.getProperty("title")});
            
// Consommation de la sortie standard de l'application externe dans un Thread separe
         new Thread() {
                public void run() {
                    try {
                        Console console = Console.getInstance();
                        InputOutput io;
                        io = console.getIo();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        String line = "";
                        try {
                            while((line = reader.readLine()) != null) {
                                // Traitement du flux de sortie de l'application si besoin est
                                io.getOut().println (line);
                            }
                        } finally {
                            reader.close();
                            io.getOut().close();
                        }
                    } catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }.start();
            
        } catch (IOException ex) {
            Console console = Console.getInstance();
            InputOutput io;
            io = console.getIo();
            io.getErr().println(ex.getMessage());
            io.getErr().close();
        }
    }
}
