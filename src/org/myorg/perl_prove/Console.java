/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.myorg.perl_prove;

import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author p-a.loriod
 */
public class Console {
    
    private static final Console me = null;
    
    static Console getInstance(){
        if(me == null){
            return new Console();
        }else{
            return me;
        }
    }
    private InputOutput io = null;
    
    private Console(){
        this.io = IOProvider.getDefault().getIO ("Prove", true);
    }
    
    public InputOutput getIo(){return this.io;}
    
}
